FROM ubuntu:20.04
LABEL description="Nginx Proxy"

ENV TZ=Asia/Jakarta

RUN apt update -y && DEBIAN_FRONTEND=noninteractive apt install -y tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt install -y certbot python3-certbot-nginx nginx cron haproxy

COPY ./ /
RUN chmod +x /entrypoint.sh

RUN mv /letsencrypt.conf /etc/nginx/snippets/letsencrypt.conf
RUN mkdir /var/www/letsencrypt
RUN chown www-data:www-data /var/www/letsencrypt
RUN mkdir /etc/nginx/ssl
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
EXPOSE 80 443
VOLUME ["/etc/nginx/sites-enabled", "/etc/letsencrypt", "/etc/haproxy", "/var/log/nginx"]
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
