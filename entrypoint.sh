#/bin/bash
rm -rf /example_certbot_task.sh
echo -n 'certbot certonly --webroot --agree-tos --no-eff-email --force-renewal -w /var/www/letsencrypt --email root@localhost -d example.com' >> /example_certbot_task.sh

mkdir -p /usr/local/etc/haproxy
cat /etc/haproxy/* > /usr/local/etc/haproxy/haproxy.cfg
haproxy -D -f /usr/local/etc/haproxy/haproxy.cfg

if [ $ENABLE_CRON == "yes" ]; then
    /usr/bin/crontab /crontab
    /usr/sbin/cron
fi

/usr/sbin/nginx -g 'daemon off;'
